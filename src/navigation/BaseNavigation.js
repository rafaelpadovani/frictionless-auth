import React from 'react';
import { connect } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SignIn from '../screens/Auth/SignIn';
import SignUp from '../screens/Auth/SignUp';
import Profile from '../screens/Profile/Profile';

const AppStack = createStackNavigator();
const AppNavigator = props => {
    return (
        <AppStack.Navigator headerMode="none" headerBackTitleVisible={false}>
            <AppStack.Screen name="Profile" component={Profile} />
        </AppStack.Navigator>
    );
}

const AuthStack = createStackNavigator();
const AuthNavigator = props => {
    return (
        <AuthStack.Navigator headerMode="none" headerBackTitleVisible={false}>
            <AuthStack.Screen name="SignIn" component={SignIn} />
            <AuthStack.Screen name="SignUp" component={SignUp} />
        </AuthStack.Navigator>
    );
}

const Stack = createStackNavigator();

const BaseNavigation = props => {
    return (
        <NavigationContainer>
            <Stack.Navigator headerMode="none" headerBackTitleVisible={false}>
                {!props.user ?
                    <Stack.Screen name="Auth" component={AuthNavigator} {...props} />
                    :
                    <Stack.Screen name="App">
                        {screenProps => <AppNavigator {...screenProps} user={props.user} completeRegister={props.completeRegister} />}
                    </Stack.Screen>
                }
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const mapStateToProps = state => ({
    user: state.auth.user
});

export default connect(mapStateToProps, {})(BaseNavigation);
