import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Text, TouchableOpacity, StyleSheet, SafeAreaView, View, StatusBar, ScrollView, Image, ActivityIndicator, Dimensions } from 'react-native';
import { PRIMARY_COLOR, BACKGROUND_COLOR } from '../../actions/types';
import { UserDetail } from '../../components';
import { logoutUser } from '../../actions';
import AntIcon from 'react-native-vector-icons/AntDesign';

const { width, height } = Dimensions.get('window');

const Profile = ({ logoutUser, user }) => {
    const [loading, setLoading] = useState(false);

    const logoutUserHandler = () => {
        setLoading(true);
        setTimeout(() => {
            logoutUser();
        }, 1500);
    }

    const overlayLoading = (
        <View style={styles.overlay}>
            <ActivityIndicator />
        </View>
    );

    if (!user) {
        return overlayLoading;
    }

    const { firstName, lastName, phone, email } = user;

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: BACKGROUND_COLOR }}>
            <StatusBar backgroundColor={PRIMARY_COLOR} barStyle='light-content' />
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1, justifyContent: 'center', backgroundColor: BACKGROUND_COLOR }}>
                <View style={{ paddingHorizontal: width * .07 }}>
                    {firstName || lastName ?
                        <UserDetail label="USER NAME" value={firstName + ' ' + lastName} />
                        : null}
                    <UserDetail label="EMAIL" value={email} />
                    <UserDetail label="PHONE" value={phone} />


                    <View style={{ height: 20 }} />
                    <TouchableOpacity style={{ alignSelf: 'flex-start', marginLeft: 20, paddingHorizontal: 10, paddingVertical: 5, /*borderRadius: 10, borderColor: PRIMARY_COLOR, borderWidth: 1*/ }} onPress={logoutUserHandler}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                            <AntIcon name="logout" size={16} color={PRIMARY_COLOR} />
                            <Text style={{ color: PRIMARY_COLOR, fontSize: 16, fontFamily: 'Ubuntu-Regular', marginVertical: 5, marginLeft: 10 }}>Log out</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>
            {loading ? overlayLoading : null}
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    overlay: {
        backgroundColor: 'black',
        // backgroundColor: APP_BACKGROUND_COLOR,
        opacity: 0.5,
        position: 'absolute',
        height,
        width,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 3
    },
});

const mapStateToProps = state => ({
    user: state.auth.user
});

// export default SignInScreen;
export default connect(mapStateToProps, { logoutUser })(Profile);