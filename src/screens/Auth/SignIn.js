import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {
    Image,
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ActivityIndicator,
    Dimensions,
    Alert
} from 'react-native';
import { Input, InputPassword, Button, ButtonSmall, Title, Link } from '../../components'
import { BACKGROUND_FORM_COLOR, PRIMARY_COLOR, WARNING_COLOR, LABEL_COLOR } from '../../actions/types'
import { CommonActions } from '@react-navigation/native';
import { logInUser } from '../../actions';
import zxcvbn from '../../assets/zxcvbn';

const { width, height } = Dimensions.get('window');

const SignIn = ({ navigation, logInUser, user, userTest, error }) => {
    const [passStrength, setPassStrength] = useState(null)
    const [loading, setLoading] = useState(false);
    const [loaded, setLoaded] = useState(false);
    const [formErrors, setFormErrors] = useState({
        email: { status: false },
        password: { status: false },
    });

    useEffect(() => {
        let timeOut = setTimeout(() => {
            setLoaded(true)
        }, 1000);
        return () => {
            clearTimeout(timeOut);
        }
    }, [])

    const [userForm, setUserForm] = useState({
        email: '',
        password: '',
    });

    const loginHandler = () => {
        const requiredFields = ['email', 'password']
        const invalid = requiredFields.find(item => userForm[item] === '' || formErrors[item].status === false);
        if (!invalid) {
            logInUser(userForm, userTest)
        } else {
            Alert.alert('Please fill in all the required fields.')
        }
    }

    const validate = (input) => {
        switch (input) {
            case 'email':
                const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
                let result = expression.test(String(userForm.email).toLowerCase())
                setFormErrors({ ...formErrors, [input]: { ...formErrors[input], status: result } });
                break;
            default:
                setFormErrors({ ...formErrors, [input]: { ...formErrors[input], status: userForm[input] !== '' } });
                break;
        }
    }

    const onChangeHandler = (input, value) => {
        setUserForm({ ...userForm, [input]: value });
        setFormErrors({ ...formErrors, [input]: { ...formErrors[input], status: false } });
        if (input === 'password') {
            let result = zxcvbn(value)
            setPassStrength(result.score)
        }
    }

    if (!loaded) {
        return (
            <View style={styles.overlay}>
                <ActivityIndicator />
            </View>
        )
    }

    const { labelStyle, row } = styles;
    const ColorText = ({ children, onPress }) => <Link onPress={onPress} customStyle={{ marginHorizontal: -10 }} customTextStyle={{ fontFamily: 'Ubuntu-Bold', color: PRIMARY_COLOR }}>{children}</Link>
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: BACKGROUND_FORM_COLOR }}>
            <StatusBar backgroundColor={PRIMARY_COLOR} barStyle='light-content' />
            <ScrollView contentContainerStyle={{ flexGrow: 1, padding: 20, backgroundColor: BACKGROUND_FORM_COLOR, paddingHorizontal: 40, justifyContent: 'center' }}>
                <Title>Welcome</Title>
                <Input
                    success={userForm.email.length > 0 && formErrors.email.status === true}
                    error={userForm.email.length > 0 && formErrors.email.status === false}
                    value={userForm.email}
                    onBlur={() => validate('email')}
                    autoCapitalize="none"
                    autoCompleteType="email"
                    placeholder="test@email.com"
                    keyboardType="email-address"
                    onChange={(text) => onChangeHandler('email', text)}
                />
                <InputPassword
                    strength={passStrength}
                    value={userForm.password}
                    onBlur={() => validate('password')}
                    placeholder="Password"
                    onChange={(text) => onChangeHandler('password', text)}
                />
                {error ?
                    <Text style={[labelStyle, { color: WARNING_COLOR, marginVertical: 0 }]}>Wrong credentials</Text>
                    : null}
                <View style={{ height: 15 }} />
                <Button onPress={loginHandler}>Log in</Button>
                <View style={{ height: 30 }} />
                <View style={row}>
                    <Text style={labelStyle}>Don't have an account?</Text><ColorText onPress={() => navigation.navigate('SignUp')}>Sign up</ColorText>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    labelStyle: {
        textAlign: 'center',
        marginVertical: 10,
        color: LABEL_COLOR,
        fontSize: 13,
        fontFamily: 'Ubuntu-Regular'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    overlay: {
        backgroundColor: 'black',
        // backgroundColor: APP_BACKGROUND_COLOR,
        opacity: 0.5,
        position: 'absolute',
        height,
        width,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 3
    }
});

const mapStateToProps = state => ({
    user: state.auth.user,
    userTest: state.auth.userTest,
    error: state.auth.error
});

export default connect(mapStateToProps, { logInUser })(SignIn);
