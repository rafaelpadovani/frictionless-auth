import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import {
    Image,
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Alert,
} from 'react-native';
import { Input, InputPassword, Button, ButtonSmall, Title, Link, RoundFloatingHeader } from '../../components'
import { BACKGROUND_FORM_COLOR, PRIMARY_COLOR, GOOGLE_COLOR, FACEBOOK_COLOR, LABEL_COLOR, WARNING_COLOR, TEXT_COLOR } from '../../actions/types'
import { signUpUser } from '../../actions';
import zxcvbn from '../../assets/zxcvbn';

const SignUp = ({ navigation, signUpUser }) => {
    const [passStrength, setPassStrength] = useState(null)
    const [formErrors, setFormErrors] = useState({
        firstName: { status: false },
        lastName: { status: false },
        email: { status: false },
        phone: { status: false },
        password: { status: false },
    });
    const [userForm, setUserForm] = useState({
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        password: '',
    });

    useEffect(() => {
        let result = zxcvbn('password')
    }, [])

    const validate = (input) => {
        switch (input) {
            case 'email':
                const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
                let result = expression.test(String(userForm.email).toLowerCase())
                setFormErrors({ ...formErrors, [input]: { ...formErrors[input], status: result } });
                break;
            default:
                setFormErrors({ ...formErrors, [input]: { ...formErrors[input], status: userForm[input] !== '' } });
                break;
        }
    }


    const onChangeHandler = (input, value) => {
        setUserForm({ ...userForm, [input]: value });
        setFormErrors({ ...formErrors, [input]: { ...formErrors[input], status: false } });
        if (input === 'password') {
            let result = zxcvbn(value)
            setPassStrength(result.score)
        }
    }

    const focusInput = (input) => {
        if (input === 'firstName' && userForm.email !== '') {
            let name = userForm.email.substring(0, userForm.email.indexOf("@"));
            name = name.charAt(0).toUpperCase() + name.slice(1);
            setUserForm({ ...userForm, [input]: name });
            setFormErrors({ ...formErrors, [input]: { ...formErrors[input], status: false } });
        }
    }

    const signUpHandler = () => {
        const requiredFields = ['email', 'phone', 'password']
        const invalid = requiredFields.find(item => userForm[item] === '' || formErrors[item].status === false);
        if (!invalid) {
            signUpUser({ ...userForm })
        } else {
            Alert.alert('Please fill in all the required fields.')
        }
    }

    const { labelStyle, row } = styles;
    const ColorText = ({ children, onPress }) => <Link onPress={onPress} customStyle={{ marginHorizontal: -10 }} customTextStyle={{ fontFamily: 'Ubuntu-Bold', color: PRIMARY_COLOR }}>{children}</Link>
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: BACKGROUND_FORM_COLOR }}>
            <StatusBar backgroundColor={BACKGROUND_FORM_COLOR} barStyle='dark-content' />
            <ScrollView contentContainerStyle={{ flexGrow: 1, padding: 20, backgroundColor: BACKGROUND_FORM_COLOR, paddingHorizontal: 35, justifyContent: 'center' }}>
                <Title>Sign up</Title>
                <Input
                    success={userForm.email.length > 0 && formErrors.email.status === true}
                    error={userForm.email.length > 0 && formErrors.email.status === false}
                    value={userForm.email}
                    onBlur={() => validate('email')}
                    autoCapitalize="none"
                    autoCompleteType="email"
                    placeholder="Email*"
                    keyboardType="email-address"
                    onChange={(text) => onChangeHandler('email', text)}
                />
                <Input
                    success={userForm.firstName.length > 0 && formErrors.firstName.status === true}
                    error={userForm.firstName.length > 0 && formErrors.firstName.status === false}
                    value={userForm.firstName}
                    onFocus={() => focusInput('firstName')}
                    onBlur={() => validate('firstName')}
                    autoCompleteType="name"
                    keyboardType="default"
                    placeholder="First Name"
                    onChange={(text) => onChangeHandler('firstName', text)}
                />
                <Input
                    success={userForm.lastName.length > 0 && formErrors.lastName.status === true}
                    error={userForm.lastName.length > 0 && formErrors.lastName.status === false}
                    value={userForm.lastName}
                    onBlur={() => validate('lastName')}
                    autoCompleteType="name"
                    keyboardType="default"
                    placeholder="Last Name"
                    onChange={(text) => onChangeHandler('lastName', text)}
                />
                <Input
                    success={userForm.phone.length > 0 && formErrors.phone.status === true}
                    error={userForm.phone.length > 0 && formErrors.phone.status === false}
                    value={userForm.phone}
                    onBlur={() => validate('phone')}
                    placeholder="Phone*"
                    keyboardType="phone-pad"
                    onChange={(text) => onChangeHandler('phone', text)}
                />
                <InputPassword
                    strength={passStrength}
                    // success={userForm.password.length > 0 && formErrors.password.status === true}
                    // error={userForm.password.length > 0 && formErrors.password.status === false}
                    value={userForm.password}
                    onBlur={() => validate('password')}
                    placeholder="Password*"
                    onChange={(text) => onChangeHandler('password', text)}
                />
                <Button onPress={signUpHandler}>Sign up</Button>
                <View style={{ height: 10 }} />
                <View style={row}>
                    <Text style={labelStyle}>Already have account?</Text><ColorText onPress={() => navigation.goBack()}>Log in</ColorText>
                </View>
            </ScrollView>
            <RoundFloatingHeader backPress={() => navigation.goBack()} />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    labelStyle: {
        textAlign: 'center',
        marginVertical: 10,
        color: LABEL_COLOR,
        fontSize: 13,
        fontFamily: 'Ubuntu-Regular'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

const mapStateToProps = state => ({
    user: state.auth.user
});

export default connect(mapStateToProps, { signUpUser })(SignUp);
