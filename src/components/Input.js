import React, { useState, useRef } from 'react';

import { Dimensions, Text, TextInput, View, StyleSheet, TouchableOpacity } from 'react-native';
import { INPUT_COLOR, TEXT_COLOR, SUCCESS_COLOR, PRIMARY_COLOR, ERROR_COLOR, PLACEHOLDER_TEXT_COLOR, LABEL_COLOR } from '../actions/types';
import AntIcon from 'react-native-vector-icons/AntDesign';

const { width, height } = Dimensions.get('window');

const Input = ({ value, onChange, placeholder, keyboardType, secureTextEntry, success, error, autoCompleteType, autoCapitalize, onBlur, onFocus }) => {
    const { InputContainerStyle, InputStyle } = styles;
    const textInputReference = useRef(null);
    return (
        <View style={InputContainerStyle}>
            <TextInput
                onFocus={onFocus}
                onBlur={onBlur}
                autoCapitalize={autoCapitalize}
                autoCompleteType={autoCompleteType}
                keyboardType={keyboardType}
                value={value}
                placeholder={placeholder}
                placeholderTextColor={PLACEHOLDER_TEXT_COLOR}
                style={InputStyle}
                secureTextEntry={secureTextEntry}
                selectionColor={PRIMARY_COLOR}
                onChangeText={(text) => onChange(text)}
                ref={textInputReference}
            />
            {success ?
                <View style={{ position: 'absolute', top: 0, bottom: 0, right: 15, justifyContent: 'center' }}>
                    <AntIcon name="checkcircle" color={SUCCESS_COLOR} size={17} />
                </View> : null}
            {error && !textInputReference.current.isFocused() ?
                <View style={{ position: 'absolute', top: 0, bottom: 0, right: 15, justifyContent: 'center' }}>
                    <AntIcon name="closecircle" color={ERROR_COLOR} size={17} />
                </View> : null}
        </View>
    )
}

const InputPassword = ({ value, onChange, placeholder, keyboardType, success, error, onBlur, strength }) => {
    const [visiblePassword, setVisiblePassword] = useState(false);
    const { InputContainerStyle, InputStyle, row, labelStyle } = styles;
    const getStrength = (number) => {
        switch (number) {
            case 0:
                return (
                    <View style={row}>
                        <View style={{ height: 3, width: 25, borderRadius: 40, backgroundColor: 'red', marginLeft: 10 }} />
                    </View>
                )
            case 1:
                return (
                    <View style={row}>
                        <View style={{ height: 3, width: 25, borderRadius: 40, backgroundColor: '#ff9966', marginLeft: 10 }} />
                        <View style={{ height: 3, width: 25, borderRadius: 40, backgroundColor: '#ff9966', marginLeft: 10 }} />
                    </View>
                )
            case 2:
                return (
                    <View style={row}>
                        <View style={{ height: 3, width: 25, borderRadius: 40, backgroundColor: '#ffcc00', marginLeft: 10 }} />
                        <View style={{ height: 3, width: 25, borderRadius: 40, backgroundColor: '#ffcc00', marginLeft: 10 }} />
                        <View style={{ height: 3, width: 25, borderRadius: 40, backgroundColor: '#ffcc00', marginLeft: 10 }} />
                    </View>
                )
            case 3:
                return (
                    <View style={row}>
                        <View style={{ height: 3, width: 25, borderRadius: 40, backgroundColor: '#99cc33', marginLeft: 10 }} />
                        <View style={{ height: 3, width: 25, borderRadius: 40, backgroundColor: '#99cc33', marginLeft: 10 }} />
                        <View style={{ height: 3, width: 25, borderRadius: 40, backgroundColor: '#99cc33', marginLeft: 10 }} />
                        <View style={{ height: 3, width: 25, borderRadius: 40, backgroundColor: '#99cc33', marginLeft: 10 }} />
                    </View>
                )
            case 4:
                return (
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ height: 3, width: 25, borderRadius: 40, backgroundColor: '#339900', marginLeft: 10 }} />
                        <View style={{ height: 3, width: 25, borderRadius: 40, backgroundColor: '#339900', marginLeft: 10 }} />
                        <View style={{ height: 3, width: 25, borderRadius: 40, backgroundColor: '#339900', marginLeft: 10 }} />
                        <View style={{ height: 3, width: 25, borderRadius: 40, backgroundColor: '#339900', marginLeft: 10 }} />
                        <View style={{ height: 3, width: 25, borderRadius: 40, backgroundColor: '#339900', marginLeft: 10 }} />
                    </View>
                )
            default:
                return (
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ height: 3, width: 25, borderRadius: 40, backgroundColor: 'red', marginLeft: 10 }} />
                    </View>
                )
        }
    }
    const getStrengthLabel = (number) => {
        if (number < 2) {
            return <Text style={[labelStyle, { color: 'red', marginVertical: 0 }]}>Weak</Text>
        }
        if (number >= 2 && number <= 3) {
            return <Text style={[labelStyle, { color: '#99cc33', marginVertical: 0 }]}>Could be better</Text>
        }
        if (number > 3) {
            return <Text style={[labelStyle, { color: '#339900', marginVertical: 0 }]}>Strong!</Text>
        }
    }
    return (
        <View>
            <View style={[InputContainerStyle, { marginBottom: 10 }]}>
                <TextInput
                    onBlur={() => onBlur()}
                    keyboardType={keyboardType}
                    value={value}
                    placeholder={placeholder}
                    placeholderTextColor={PLACEHOLDER_TEXT_COLOR}
                    style={InputStyle}
                    secureTextEntry={!visiblePassword}
                    selectionColor={PRIMARY_COLOR}
                    onChangeText={(text) => onChange(text)}
                />
                {success ?
                    <View style={{ position: 'absolute', top: 0, bottom: 0, right: 15, justifyContent: 'center' }}>
                        <AntIcon name="checkcircle" color={SUCCESS_COLOR} size={17} />
                    </View> : null}
                {error ?
                    <View style={{ position: 'absolute', top: 0, bottom: 0, right: 15, justifyContent: 'center' }}>
                        <AntIcon name="closecircle" color={ERROR_COLOR} size={17} />
                    </View> : null}
                {visiblePassword ?
                    <TouchableOpacity onPress={() => setVisiblePassword(visiblePassword => !visiblePassword)} style={{ position: 'absolute', top: 0, bottom: 0, right: 15, justifyContent: 'center' }}>
                        <AntIcon name="eye" color={TEXT_COLOR} size={17} />
                    </TouchableOpacity>
                    :
                    <TouchableOpacity onPress={() => setVisiblePassword(visiblePassword => !visiblePassword)} style={{ position: 'absolute', top: 0, bottom: 0, right: 15, justifyContent: 'center' }}>
                        <AntIcon name="eyeo" color={PLACEHOLDER_TEXT_COLOR} size={20} />
                    </TouchableOpacity>
                }
            </View>
            <View style={{ marginHorizontal: 15 }}>
                {strength !== null && value !== '' ?
                    <View style={[row, { justifyContent: 'space-between' }]}>
                        {getStrength(strength)}
                        {getStrengthLabel(strength)}
                    </View>
                    :
                    <View style={{ height: 15 }} />}
            </View>
        </View>
    )
}

const UserDetail = props => {
    return (
        <View style={{ paddingHorizontal: 20, marginTop: 15 }}>
            <Text style={{ fontFamily: 'Ubuntu-Bold', fontSize: 11, color: "#CCCCCC", marginTop: 5 }}>{props.label}</Text>
            <Text style={{ fontFamily: 'Ubuntu-Bold', fontSize: 14, color: TEXT_COLOR, marginVertical: 5 }}>
                {(props.value?.length > width * .09) ?
                    props.value?.substr(0, width * .08) + '...' :
                    props.value
                }
            </Text>
            <View style={{ backgroundColor: TEXT_COLOR, width: '100%', height: 0.5, opacity: 0.5, marginTop: 2, marginBottom: 5 }} />
        </View>
    )
}


const styles = StyleSheet.create({
    labelStyle: {
        textAlign: 'center',
        color: LABEL_COLOR,
        fontSize: 11,
        fontFamily: 'Ubuntu-Regular'
    },
    InputContainerStyle: {
        marginVertical: 15,
        height: 50,
        borderRadius: 28,
        backgroundColor: INPUT_COLOR,
        paddingHorizontal: 30,
        paddingRight: 35,
        justifyContent: 'center',
        elevation: 4,
    },
    InputStyle: {
        flex: 1,
        color: TEXT_COLOR,
        fontSize: 14,
        fontFamily: "Ubuntu-Regular",
        textAlign: 'left'
    },
    row: { flexDirection: 'row', alignItems: 'center' }
})

export { Input, InputPassword, UserDetail };