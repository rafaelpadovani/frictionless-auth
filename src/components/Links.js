import React from 'react';

import { Text, StyleSheet, TouchableOpacity } from 'react-native';
import { TEXT_COLOR, LINK_COLOR } from '../actions/types';

const Link = ({ children, customStyle, customTextStyle, onPress }) => {
    const { titleStyle } = styles;
    return (
        <TouchableOpacity onPress={onPress} style={customStyle}>
            <Text style={[titleStyle, customTextStyle]}>{children}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    titleStyle: {
        marginHorizontal: 15,
        color: LINK_COLOR,
        fontSize: 13,
        fontFamily: 'Ubuntu-Regular'
    },
})

export { Link };