import React from 'react';

import {
    Text,
    TouchableOpacity,
    Image,
    View,
    StyleSheet,
    StatusBar,
    Dimensions,
    Platform
} from 'react-native';
import { PRIMARY_COLOR } from '../actions/types';
import Icon from 'react-native-vector-icons/AntDesign';

const { width, height } = Dimensions.get('window');

const RoundFloatingHeader = props => {
    const { RoundFloatingHeader } = styles;
    return (
        <TouchableOpacity
            style={[RoundFloatingHeader, props.customBackButtomStyle]}
            onPress={() => props.backPress()}>
            <Icon name="arrowleft" color={PRIMARY_COLOR} size={15} />
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    RoundFloatingHeader: {
        position: 'absolute',
        alignItems: 'center',
        left: 15,
        justifyContent: 'center',
        marginTop: height >= 812 ? 50 : 35,
        width: 45,
        height: 45,
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 25,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.05,
        shadowRadius: 8.84,
        elevation: 4,
    }
});

export { RoundFloatingHeader };