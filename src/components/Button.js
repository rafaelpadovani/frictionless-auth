import React from 'react';
import { StyleSheet, TouchableWithoutFeedback, Platform, Animated, Easing, Text } from 'react-native';
import { BUTTON_COLOR } from '../actions/types';

const Button = ({ onPress, children, customStyle }) => {
    let scaleValue = new Animated.Value(0);
    const cardScale = scaleValue.interpolate({
        inputRange: [0, 0.4, 0.6, 1],
        outputRange: [1, 0.96, 0.96, 1]
    });
    const { buttonStyle, textStyle } = styles;
    let transformStyle = { ...buttonStyle, transform: [{ scale: cardScale }], ...customStyle };
    return (
        <TouchableWithoutFeedback onPressIn={() => {
            scaleValue.setValue(0);
            Animated.timing(scaleValue, {
                toValue: 1,
                duration: 250,
                easing: Easing.linear,
                useNativeDriver: true
            }).start();
        }}
            onPressOut={onPress}>
            <Animated.View style={transformStyle}>
                <Text style={textStyle}>{children}</Text>
            </Animated.View>
        </TouchableWithoutFeedback>
    );
};


const styles = StyleSheet.create({
    buttonStyle: {
        marginVertical: 20,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: BUTTON_COLOR,
        height: 50,
        width: '100%',
        borderRadius: 25,
        alignSelf: 'center',
        elevation: 8,
    },
    textStyle: {
        alignSelf: 'center',
        color: '#ffffff',
        fontSize: 16,
        paddingTop: Platform.OS !== 'ios' ? 8 : 10,
        paddingBottom: 10,
        padding: 5,
        fontFamily: 'Ubuntu-Bold'
    }
})

export { Button };