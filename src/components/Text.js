import React from 'react';

import { Text, StyleSheet } from 'react-native';
import { TEXT_COLOR } from '../actions/types';

const Title = ({ children, customStyle }) => {
    const { titleStyle } = styles;
    return (
        <Text style={[titleStyle, customStyle]}>{children}</Text>
    )
}

const styles = StyleSheet.create({
    titleStyle: {
        margin: 10,
        textAlign: 'center',
        color: TEXT_COLOR,
        fontSize: 35,
        paddingTop: Platform.OS !== 'ios' ? 8 : 10,
        paddingBottom: 10,
        fontFamily: 'Ubuntu-Medium'
    },
})

export { Title };