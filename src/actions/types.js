export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';
export const LOGOUT_USER = 'logout_user';

export const REGISTER_USER = 'register_user';
export const REGISTER_USER_SUCCESS = 'register_user_success';
export const REGISTER_USER_FAIL = 'register_user_fail';

export const PRIMARY_COLOR = '#6fd3a8';
export const INPUT_COLOR = '#FFFFFF';
export const TEXT_COLOR = '#23233C';
export const PLACEHOLDER_TEXT_COLOR = '#C7C7CD';
export const LINK_COLOR = '#8D8D8D';
export const LABEL_COLOR = '#8D8D8D';
export const SUCCESS_COLOR = '#6CC57C';
export const WARNING_COLOR = '#D13838';
export const ERROR_COLOR = '#EE6A6E';
export const BUTTON_COLOR = '#6fd3a8';
export const GOOGLE_COLOR = '#DB4437';
export const FACEBOOK_COLOR = '#4267B2';
export const BACKGROUND_COLOR = '#FFFFFF';
export const BACKGROUND_FORM_COLOR = '#F4F5FA';
