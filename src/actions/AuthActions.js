import {
    REGISTER_USER,
    LOGIN_USER,
    LOGIN_USER_FAIL,
    LOGOUT_USER
} from '../actions/types';

export const signUpUser = (form) => {
    return {
        type: REGISTER_USER,
        payload: form
    };
}

export const logInUser = (form, userTest) => {
    if (form.email === userTest.email && form.password === userTest.password) {
        return {
            type: LOGIN_USER,
            payload: userTest
        };
    }
    return {
        type: LOGIN_USER_FAIL,
        payload: ''
    };
}

export const logoutUser = () => {
    return {
        type: LOGOUT_USER,
        payload: ''
    };
};