import { combineReducers } from 'redux';
import persistReducer from './AuthReducer';

export default combineReducers({
    auth: persistReducer
});