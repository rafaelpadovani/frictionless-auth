import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';

import {
    LOGIN_USER_FAIL,
    LOGIN_USER,
    LOGOUT_USER,
    REGISTER_USER,
} from '../actions/types';

const INITIAL_STATE = {
    email: '',
    password: '',
    user: null,
    userTest: {
        email: 'user@email.com',
        password: '1234',
        phone: '9999999999',
        firstName: 'Name',
        lastName: 'Surname'
    },
    error: '',
    errorLogin: '',
    errorUserDataRegister: '',
    loading: false,
};

const AuthReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LOGIN_USER:
            return { ...state, ...INITIAL_STATE, user: action.payload };
        case LOGIN_USER_FAIL:
            return { ...state, error: true };
        case LOGOUT_USER:
            return { ...state, ...INITIAL_STATE };
        case REGISTER_USER:
            return { ...state, ...INITIAL_STATE, user: action.payload };
        default:
            return state;
    }
};

const persistConfig = {
    key: 'auth',
    storage: AsyncStorage,
    whitelist: ['user']
};

export default persistReducer(persistConfig, AuthReducer);
