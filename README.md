# frictionless-

## How to run

Assuming that you followed the - [React Native CLI Quickstart](https://reactnative.dev/docs/environment-setup) setup:

### Run:

`npm install`

## iOS:

`npx pod-install ios`

If you get errors installing the pods delete the `ios/Podfile.lock` file and try again.

Open `kollexapp.xcworkspace` project file.

Run `npx react-native start` and press 'play' button on Xcode.

## Android:

Inside android folder create a file called `local.properties` specifying the path where your Android SDK is installed, it should look like this:

`sdk.dir=/path/to/your/Android/sdk`

Run `npx react-native start`

Open Android Studio to run it or make sure you have a simulator or Android device connected to your computer and on another tab of the terminal run:

Run `npx react-native run-android`

### Login Test

Existing user to test:

email: user@email.com

password: 1234
