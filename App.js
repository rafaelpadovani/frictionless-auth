import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import BaseNavigation from './src/navigation/BaseNavigation';

const persistConfig = {
  key: 'AuthLoading',
  storage: AsyncStorage,
};

const pReducer = persistReducer(persistConfig, reducers);

const store = compose(
  applyMiddleware(ReduxThunk)
)(createStore)(pReducer);

const persistor = persistStore(store);

const App = () => {
  return (
    <Provider store={store}>
      <BaseNavigation screenProps={store}>

      </BaseNavigation>
    </Provider>
  );
};

export default App;
